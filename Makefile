# Makefile for OpenCraft handbook.
all: install_prereqs quality compile

compile:
	mkdocs build

install_prereqs:
	pip install --user poetry
	poetry install

quality:
	make quality-markdown

quality-markdown:
	npx markdownlint-cli docs

run:
	mkdocs serve

clean:
	rm -rvf public
