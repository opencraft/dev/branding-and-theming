# Goals

## Reduced Maintenance Burden

A large amount of effort is currently involved in upgrading client themes from
one release to another. Getting consistent styling across all pages is a pain
and requires significant manual QA.

One of the aims of this project is to work towards a new comprehensive theming
system for Open edX that significantly reduces the maintenance burden for
updating themes across releases. The target is to achieve a 90% reduction in
the effort involved in maintaining themes across releases.

## Deeper Customisation

Since the largest amount of maintenance is required for themes that have the
most amount of customisation, another goal of this comprehensive theming system
is to allow deeper customisation. For instance it should be possible to modify
the [learning frontend] to support a sequence navigation UX where the next unit
load automatically on scroll instead of needing to click back / next.

## Open Source UI for Theme Customisation

Out of the box, Open edX doesn't include any UI that allows customising the
theme. There are a number of proprietary solution, and OpenCraft has an open
source tool that is integrated with the [OpenCraft Instance Manager] (aka OCIM an
automated Open edX deployment and sandbox tool).

![OpenCraft Instance Manager customising button styles](
    images/ocim-theme-customise-button.png
)
OpenCraft Instance Manager customising button styles

As part of this project we'd like to help create an open source standalone
theming UI that allows customising different aspects of the platform using a
friendly interface. It can be based on the existing code we already have in
OCIM.

## Near-Real-Time Theme Previews

Currently, the time between applying a theme change and seeing it live is too
long and this discourages experimentation. With MFEs there is a great
opportunity to support a much quicker preview workflow.

We would like to support a automated system for quickly building and deploying
themes for preview such that users can see results quickly, and experiment more
before applying their desired changes to production.

## Quick Deployment of Theme Changes

Once users have a preferred theme style ready, we'd like to support an
automated deployment mechanism that redeploys all MFEs with the theme changes.
This deployment system will ideally be agnostic of whether it is running on
Google, Amazon or some other infrastructure.

[OpenCraft Instance Manager]: https://github.com/open-craft/opencraft
[learning frontend]: https://github.com/edx/frontend-app-learning
